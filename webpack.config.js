module.exports = {
    entry: './src/app.js',
    output: {
        path: 'www/js',
        filename: 'index.js'
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: 'style!css'  },
            { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader' }
        ]
    }
}
