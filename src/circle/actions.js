export const INITIALIZE = 'INITIALIZE' 
export const MOVE_DOT = 'MOVE_DOT'

export function initialize(data) {
    return { type: INITIALIZE, data }
}

export function moveDot(data) {
    return { type: MOVE_DOT, data }
}
