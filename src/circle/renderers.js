import Rx from 'rx'

import { moveDot } from './actions'
import { getData, circleIsATarget } from './utils'

export function render(svg, store) {
    renderCircle(svg, store)
}

export function renderCircle(svg, store) {
    const state = store.getState()
    const circle = svg.selectAll('circle').data([state.get('data')])

    circle.enter()
        .append('circle')
        .attr({
            'cx': d => d.cx ,
            'cy': d => d.cy,
            'r': d => d.r
        })
        .style('fill', d => d.color)
        .each(() => {
            const touch$ = Rx.Observable.fromEvent(svg.node(), 'touchmove')
                .scan((pair, event) => {
                    return {
                        first: event,
                        second: pair.first
                    }
                })
                .filter((pair) => {
                    if (circleIsATarget(pair.first) === true &&
                           circleIsATarget(pair.second) === false) {
                        return true
                    }

                    return false
                })

            const click$ = Rx.Observable.fromEvent(circle.node(), 'click')
            const catch$ = touch$.merge(click$)

            catch$.subscribe(() => {
                store.dispatch(moveDot(getData(svg, store.getState())))
            })
        })


    circle.transition()
        .attr({
            'cx': d => d.cx ,
            'cy': d => d.cy,
            'r': d => d.r,
            'id': 'update'
        })
        .style('fill', d => d.color)
}
