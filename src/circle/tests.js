import { test } from 'tape'
import { Map } from 'immutable'

import { initialize, moveDot } from './actions'
import { catchADot } from './reducers'
import { isNear } from './utils'

test('test actions', t => {
    const data = { cx: 1, cy: 2, r: 3 }

    let state = catchADot(Map(), initialize(data))
    t.true(state.equals(Map({data})))

    state = catchADot(Map(), moveDot(data))
    t.true(state.equals(Map({data})))

    state = catchADot(Map(), {type: 'abc'})
    t.true(state.equals(Map()))

    state = catchADot(undefined, {type: 'abc'})
    t.true(state.equals(Map()))
 
    t.end()
})

test('test isNear', t => {
    let a = { cx: 1, cy: 2, r: 3 }
    let b = { cx: 2, cy: 2, r: 3 }
    t.true(isNear(a,b))

    b = { cx: 7, cy: 2, r: 3 }
    t.false(isNear(a,b))

    t.end()
})
