import d3 from 'd3'

export function getData(svg, state) {
    const colorScale = d3.scale.category10()
    const r = 50 + Math.random() * 50
    const data = {
        'r': r,
        'cx': r + Math.random() * (svg.attr('width') - 2*r),
        'cy': r + Math.random() * (svg.attr('height') - 2*r),
        'color': colorScale.range()[Math.round(Math.random() * 10)]
    }
    while (state !== undefined &&  isNear(state.get('data'), data)) {
        data.cx = r + Math.random() * (svg.attr('width') - 2*r)
        data.cy = r + Math.random() * (svg.attr('height') - 2*r)
    }

    return data
}

export function isNear(a, b) {
    const distance = Math.sqrt(
        Math.pow(Math.abs(a.cx - b.cx), 2) +
        Math.pow(Math.abs(a.cy - b.cy), 2)
    )

    return distance < (a.r + b.r)
}

export function circleIsATarget(event) {
    if (event === undefined) {
        return false
    }

    for (let i = 0, len = event.targetTouches.length; i < len; i++) {
        const x = event.targetTouches[i].clientX
        const y = event.targetTouches[i].clientY
        const target = document.elementFromPoint(x, y)
        if (target.nodeName === 'circle') {
            return true
        }
    }

    return false
}
