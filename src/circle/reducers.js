import { Map } from 'immutable'
import { INITIALIZE, MOVE_DOT } from './actions'

export function catchADot(state = Map(), action) {
    switch (action.type) {
    case INITIALIZE:
        return state.set('data', action.data)
    case MOVE_DOT:
        return state.set('data', action.data)
    default:
        return state
    }
}
