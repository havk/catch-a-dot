import d3 from 'd3'

import { createStore, compose } from 'redux'
import { catchADot } from './circle/reducers'
import { initialize } from './circle/actions'
import { getData } from './circle/utils'
import { render } from './circle/renderers'

function App(self={}) {
    const init = function () {
        self.store = createStore(
            catchADot,
            compose(window.devToolsExtension ? window.devToolsExtension() : f => f)
        )

        document.addEventListener('deviceready', onDeviceReady, false)
    }

    const createSVG = function() {
        const svg = d3.select('.app')
            .append('svg')
                .attr({
                    'width': window.innerWidth,
                    'height': window.innerHeight * 0.99
                })

        return svg
    }

    const onDeviceReady = function() {
        window.OnLoadHandler.pageLoaded()
        self.svg = createSVG()
        self.store.subscribe(() => render(self.svg, self.store))
        self.store.dispatch(initialize(getData(self.svg)))
    }

    return Object.freeze({
        init
    })
}

const app = App()
app.init()
